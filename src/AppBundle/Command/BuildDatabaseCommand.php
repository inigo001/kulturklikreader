<?php
namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\Progress;

use AppBundle\Entity\Event;
use AppBundle\Entity\EventType;
use AppBundle\Entity\Language;
use AppBundle\Entity\Country;
use AppBundle\Entity\Territory;
use AppBundle\Entity\Town;
use AppBundle\Entity\Place;

use Symfony\Component\Validator\Constraints\DateTime;

use Doctrine\Common\Collections\ArrayCollection;

class BuildDatabaseCommand extends ContainerAwareCommand
{

    const KULTURKLIK_JSON_URL = "http://opendata.euskadi.eus/contenidos/ds_eventos/agenda_cultura_euskadi/es_kultura/adjuntos/kulturklik.json";

    //const KULTURKLIK_JSON_URL = "/mnt/d/Trabajos Actuales/KulturKlikReader/kulturklik.json";

    // Funcion para decodificar el JSON que nose llega, ya que esta en formato JSONP y da problemas
    private function jsonp_decode($jsonp, $assoc = false) 
    { 

        if($jsonp[0] !== '[' && $jsonp[0] !== '{') 
        { 
            $jsonp = substr($jsonp, strpos($jsonp, '('));
        }

        return json_decode(trim($jsonp,'();'), $assoc);
    }

    private function retrieveJSON( OutputInterface $output )
    {
        $progress = new ProgressBar($output);

        $ctx = stream_context_create(array(), array('notification' => function ($notification_code, $severity, $message, $message_code, $bytes_transferred, $bytes_max) use ($output, $progress) 
        {

            switch ($notification_code) 
            {
                case STREAM_NOTIFY_FILE_SIZE_IS:
                    $progress->start($bytes_max);
                    break;
                case STREAM_NOTIFY_PROGRESS:
                    $progress->setProgress($bytes_transferred);
                    break;
            }
        }));

        $json = file_get_contents($this::KULTURKLIK_JSON_URL, false, $ctx);
        $progress->finish();

        return $this->jsonp_decode($json);

    }

    private function createLanguage( $document_ )
    {
        $newLanguage = new Language();
        $newLanguage->setName( $document_->eventLanguages );

        return $newLanguage;
    }

    private function createEventType( $document_ )
    {
        $newEventType = new EventType();
        $newEventType->setName( $document_->eventType );

        return $newEventType;
    }

    private function createTown( $document_ , $territoryObject_ , $countryObject_ )
    {
        $newTown = new Town();
        $newTown->setName( $document_->municipality);
        $newTown->setCode( $document_->municipalitycode);
        $newTown->setTerritory( $territoryObject_ );
        $newTown->setCountry( $countryObject_ );

        return $newTown;
    }

    private function createCountry( $document_ )
    {
        $newCountry = new Country();
        $newCountry->setName( $document_->country );
        $newCountry->setCode( $document_->countrycode );

        return $newCountry;
    }

    private function createTerritory( $document_ , $countryObject_ )
    {
        $newTerritory = new Territory();
        $newTerritory->setName( $document_->territory );
        $newTerritory->setCode( $document_->territorycode );
        $newTerritory->setCountry( $countryObject_ );

        return $newTerritory;
    }

    private function createPlace( $document_ , $townObject_ )
    {

        $newPlace = new Place();
        $newPlace->setName( $document_->placename);
        $newPlace->setLatitude( $document_->latwgs84 );
        $newPlace->setLongitude( $document_->lonwgs84 );
        $newPlace->setTown( $townObject_ );

        return $newPlace;
    }

    private function createEvent( $document_ , $placeObject_ , $eventTypeObject_ , $languageObject_ )
    {

        $event = new Event();

        $event->setName( $document_->documentName );
        $event->setOnline( false );
        $event->setPrice( array(0,1) );
        $event->setEventStartDate( \DateTime::createFromFormat('d/m/Y', $document_->eventStartDate ) );
        $event->setEventEndDate( \DateTime::createFromFormat('d/m/Y', $document_->eventEndDate ) );
        $event->setEventTimeTable( \DateTime::createFromFormat('H:i', '12:30') );
        $event->setEventRegistrationStart( \DateTime::createFromFormat('d/m/Y', $document_->eventRegistrationStartDate )  );
        $event->setEventRegistrationEnd( \DateTime::createFromFormat('d/m/Y', $document_->eventRegistrationEndDate ));
        $event->setFriendlyURL( $document_->friendlyUrl );
        $event->setPhysicalURL( $document_->physicalUrl  );
        $event->setDataXML( $document_->dataXML);
        $event->setMetadataXML( $document_->metadataXML );
        $event->setZipFile( $document_->zipFile );
        $event->setPlace( $placeObject_ );
        $event->setEventType( $eventTypeObject_ );
        $event->setLanguage( $languageObject_ );

        return $event;

    }

    protected function configure()
    {
        // Nombre
        $this->setName('app:build-database');
        // Descripcion corta
        $this->setDescription('Crea una DB cogiendo los datos de Kulturklik');
        // Descripcion larga
        $this->setHelp('Descarga un .json desde opendata Euskadi y despues lo procesa. ... ...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        //inicio
        $output->
            writeln('<info>Download Start</>');

        // recogemos el JSON
        $json = $this->retrieveJSON( $output );

        //terminamos de descargar
        $output->
            writeln('<info>Download Finish</>');


        $hasLanguage = function($name) 
        {
            return function(Language $lang) use ($name) 
            {
                return $name == $lang->getName();
            };
        };

        $hasEventType = function($name) 
        {
            return function(EventType $event) use ($name) 
            {
                return $name == $event->getName();
            };
        };

        $hasTown = function($code) 
        {
            return function(Town $town) use ($code) 
            {
                return $code == $town->getCode();
            };
        };

        $hasTerritory = function($code) 
        {
            return function(Territory $territory) use ($code) 
            {
                return $code == $territory->getCode();
            };
        };

        $hasCountry = function($code) 
        {
            return function(Country $country) use ($code) 
            {
                return $code == $country->getCode();
            };
        };

        $hasPlace = function($name , $town) 
        {
            return function(Place $place) use ($name, $town) 
            {
                return ( $name == $place->getName() ) && ( $town == $place->getTown() ) ;
            };
        };


        $languages = new ArrayCollection();
        $eventTypes = new ArrayCollection();
        $events= new ArrayCollection();
        $towns= new ArrayCollection();
        $territories= new ArrayCollection();
        $countries= new ArrayCollection();
        $places= new ArrayCollection();


        for ( $i = 0 ; $i <= count($json) - 1 ; ++$i )
        {

            $document = $json[$i];

            $languageObject = NULL;

            // COMPROBAMOS IDIOMA
            if ( property_exists( $document, 'eventLanguages') )
            {

                $languageObject = $languages->filter( $hasLanguage( $document->eventLanguages ) )->first();

                if ( $languageObject == NULL )
                {

                    $languageObject = $this->createLanguage( $document );
                    $languages->add($languageObject);

                }

            }

            // COMPROBAMOS EVENTTYPE

            $eventTypeObject = NULL;
 
            if ( property_exists( $document, 'eventType') )
            {

                $eventTypeObject = $eventTypes->filter( $hasEventType( $document->eventType ) )->first();

                if ( $eventTypeObject == NULL )
                {
                    $eventTypeObject = $this->createEventType( $document );
                    $eventTypes->add($eventTypeObject);
                }
                 
            }

            $townObject = NULL;

            // COMPROBAMOS TOWN

            if ( property_exists( $document, 'municipality') )
            {

                $townObject = $towns->filter($hasTown($document->municipalitycode))->first();

                if ( $townObject == NULL )
                {

                    // COUNTRY

                    $countryObject = NULL;

                    if ( property_exists( $document, 'countrycode') )
                    {

                        $countryObject = $countries->filter($hasCountry($document->countrycode))->first();

                        if ( $countryObject == NULL)
                        {

                            $countryObject = $this->createCountry($document);
                            $countries->add($countryObject);
     
                        }

                    }

                    // TERRITORY

                    $territoryObject = NULL;

                    if ( property_exists( $document, 'territorycode') )
                    {

                        $territoryObject = $territories->filter($hasTerritory($document->territorycode))->first();

                        if ( $territoryObject == NULL)
                        {

                            $territoryObject = $this->createTerritory( $document , $countryObject );
                            $territories->add($territoryObject);
                            
                        }

                    }

                    $townObject = $this->createTown( $document , $territoryObject , $countryObject );
                    $towns->add($townObject);      

                }

            }

            $placeObject = NULL;

            if ( property_exists( $document, 'placename')  )
            {

                $placeObject = $places->filter($hasPlace( $document->placename, $townObject ))->first();

                if ( $placeObject == NULL )
                {

                    $placeObject = $this->createPlace($document , $townObject );
                    $places->add($placeObject);
                }

            }

            $event = $this->createEvent( $document ,  $placeObject , $eventTypeObject , $languageObject );

            $events->add($event);

        }

        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getEntityManager();




        foreach( $languages as $lang )
        {
            $em->persist($lang);
        }

        foreach( $eventTypes as $type )
        {
            $em->persist($type);
        }

        foreach( $events as $event )
        {
            $em->persist($event);
        }

        foreach( $countries as $country )
        {
            $em->persist($country);
        }

        foreach( $territories as $territory )
        {
            $em->persist($territory);
        }

        foreach( $towns as $town )
        {
            $em->persist($town);
        }

        foreach( $places as $place )
        {
            $em->persist($place);
        }

        //$em->flush();

    }


}

?>