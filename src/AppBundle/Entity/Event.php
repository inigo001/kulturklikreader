<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="online", type="boolean")
     */
    private $online;

    /**
     * @var array
     *
     * @ORM\Column(name="price", type="array")
     */
    private $price;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventStartDate", type="date")
     */
    private $eventStartDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventEndDate", type="date")
     */
    private $eventEndDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventTimeTable", type="time")
     */
    private $eventTimeTable;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventRegistrationStart", type="date")
     */
    private $eventRegistrationStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="eventRegistrationEnd", type="date")
     */
    private $eventRegistrationEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="friendlyURL", type="string", length=255)
     */
    private $friendlyURL;

    /**
     * @var string
     *
     * @ORM\Column(name="physicalURL", type="string", length=255)
     */
    private $physicalURL;

    /**
     * @var string
     *
     * @ORM\Column(name="DataXML", type="string", length=255)
     */
    private $dataXML;

    /**
     * @var string
     *
     * @ORM\Column(name="metadataXML", type="string", length=255)
     */
    private $metadataXML;

    /**
     * @var string
     *
     * @ORM\Column(name="zipFile", type="string", length=255)
     */
    private $zipFile;

    /**
     * Many Events have One Place.
     * @ORM\ManyToOne(targetEntity="Place", inversedBy="events")
     */
    private $place;

    /**
     * Many Events have One Language.
     * @ORM\ManyToOne(targetEntity="Language", inversedBy="events")
     */
    private $language;

    /**
     * Many Events have One EventType.
     * @ORM\ManyToOne(targetEntity="EventType", inversedBy="events")
     */
    private $eventType;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set online
     *
     * @param boolean $online
     *
     * @return Event
     */
    public function setOnline($online)
    {
        $this->online = $online;

        return $this;
    }

    /**
     * Get online
     *
     * @return bool
     */
    public function getOnline()
    {
        return $this->online;
    }

    /**
     * Set price
     *
     * @param array $price
     *
     * @return Event
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return array
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set eventStartDate
     *
     * @param \DateTime $eventStartDate
     *
     * @return Event
     */
    public function setEventStartDate($eventStartDate)
    {
        $this->eventStartDate = $eventStartDate;

        return $this;
    }

    /**
     * Get eventStartDate
     *
     * @return \DateTime
     */
    public function getEventStartDate()
    {
        return $this->eventStartDate;
    }

    /**
     * Set eventEndDate
     *
     * @param \DateTime $eventEndDate
     *
     * @return Event
     */
    public function setEventEndDate($eventEndDate)
    {
        $this->eventEndDate = $eventEndDate;

        return $this;
    }

    /**
     * Get eventEndDate
     *
     * @return \DateTime
     */
    public function getEventEndDate()
    {
        return $this->eventEndDate;
    }

    /**
     * Set eventTimeTable
     *
     * @param \DateTime $eventTimeTable
     *
     * @return Event
     */
    public function setEventTimeTable($eventTimeTable)
    {
        $this->eventTimeTable = $eventTimeTable;

        return $this;
    }

    /**
     * Get eventTimeTable
     *
     * @return \DateTime
     */
    public function getEventTimeTable()
    {
        return $this->eventTimeTable;
    }

    /**
     * Set eventRegistrationStart
     *
     * @param \DateTime $eventRegistrationStart
     *
     * @return Event
     */
    public function setEventRegistrationStart($eventRegistrationStart)
    {
        $this->eventRegistrationStart = $eventRegistrationStart;

        return $this;
    }

    /**
     * Get eventRegistrationStart
     *
     * @return \DateTime
     */
    public function getEventRegistrationStart()
    {
        return $this->eventRegistrationStart;
    }

    /**
     * Set eventRegistrationEnd
     *
     * @param \DateTime $eventRegistrationEnd
     *
     * @return Event
     */
    public function setEventRegistrationEnd($eventRegistrationEnd)
    {
        $this->eventRegistrationEnd = $eventRegistrationEnd;

        return $this;
    }

    /**
     * Get eventRegistrationEnd
     *
     * @return \DateTime
     */
    public function getEventRegistrationEnd()
    {
        return $this->eventRegistrationEnd;
    }

    /**
     * Set friendlyURL
     *
     * @param string $friendlyURL
     *
     * @return Event
     */
    public function setFriendlyURL($friendlyURL)
    {
        $this->friendlyURL = $friendlyURL;

        return $this;
    }

    /**
     * Get friendlyURL
     *
     * @return string
     */
    public function getFriendlyURL()
    {
        return $this->friendlyURL;
    }

    /**
     * Set physicalURL
     *
     * @param string $physicalURL
     *
     * @return Event
     */
    public function setPhysicalURL($physicalURL)
    {
        $this->physicalURL = $physicalURL;

        return $this;
    }

    /**
     * Get physicalURL
     *
     * @return string
     */
    public function getPhysicalURL()
    {
        return $this->physicalURL;
    }

    /**
     * Set dataXML
     *
     * @param string $dataXML
     *
     * @return Event
     */
    public function setDataXML($dataXML)
    {
        $this->dataXML = $dataXML;

        return $this;
    }

    /**
     * Get dataXML
     *
     * @return string
     */
    public function getDataXML()
    {
        return $this->dataXML;
    }

    /**
     * Set metadataXML
     *
     * @param string $metadataXML
     *
     * @return Event
     */
    public function setMetadataXML($metadataXML)
    {
        $this->metadataXML = $metadataXML;

        return $this;
    }

    /**
     * Get metadataXML
     *
     * @return string
     */
    public function getMetadataXML()
    {
        return $this->metadataXML;
    }

    /**
     * Set zipFile
     *
     * @param string $zipFile
     *
     * @return Event
     */
    public function setZipFile($zipFile)
    {
        $this->zipFile = $zipFile;

        return $this;
    }

    /**
     * Get zipFile
     *
     * @return string
     */
    public function getZipFile()
    {
        return $this->zipFile;
    }

    /**
     * Set place
     *
     * @param \AppBundle\Entity\Place $place
     *
     * @return Event
     */
    public function setPlace(\AppBundle\Entity\Place $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * Get place
     *
     * @return \AppBundle\Entity\Place
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     *
     * @return Event
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set eventType
     *
     * @param \AppBundle\Entity\EventType $eventType
     *
     * @return Event
     */
    public function setEventType(\AppBundle\Entity\EventType $eventType = null)
    {
        $this->eventType = $eventType;

        return $this;
    }

    /**
     * Get eventType
     *
     * @return \AppBundle\Entity\EventType
     */
    public function getEventType()
    {
        return $this->eventType;
    }
}
