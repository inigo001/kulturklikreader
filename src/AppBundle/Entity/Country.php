<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * Country
 *
 * @ORM\Table(name="country")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CountryRepository")
 */
class Country
{

    /**
     * One Country has Many Territories.
     * @ORM\OneToMany(targetEntity="Territory", mappedBy="country")
     */
    private $territories;

    /**
     * One Country has Many Towns.
     * @ORM\OneToMany(targetEntity="Town", mappedBy="country")
     */
    private $towns;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="code", type="integer", unique=true)
     */
    private $code;

    public function __construct() 
    {
        $this->territories = new ArrayCollection();
        $this->towns = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param integer $code
     *
     * @return Country
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Add territory
     *
     * @param \AppBundle\Entity\Territory $territory
     *
     * @return Country
     */
    public function addTerritory(\AppBundle\Entity\Territory $territory)
    {
        $this->territories[] = $territory;

        return $this;
    }

    /**
     * Remove territory
     *
     * @param \AppBundle\Entity\Territory $territory
     */
    public function removeTerritory(\AppBundle\Entity\Territory $territory)
    {
        $this->territories->removeElement($territory);
    }

    /**
     * Get territories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTerritories()
    {
        return $this->territories;
    }
}
