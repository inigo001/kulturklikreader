KulturKlikReader
======================

composer update

php bin/console app:build-database


El sistema descarga de la web de opendata.euskadi el fichero JSONP correspondiente a los eventos culturales, lo procesa para convertirlo en un objeto de PHP y después realiza los procesos pertinentes para repartir los datos en distintas tablas.

La DB está compuesta de 7 tablas:

Country: Guarda el nombre del país y su código.
Territory: Guarda el nombre del territorio, su código y el país asociado mediante una relación ManyToOne.
Town: Guarda el nombre del pueblo, el territorio en el que se encuentra (puede ser nulo) y el país, también de manera relacional.
Place: Guarda el nombre del lugar (el recinto del evento), su latitud y longitud y el pueblo en el que se encuentra.
Language: El nombre del idioma.
EventType: El tipo de evento.
Event: El evento, su nombre, fechas y relaciones con Language, EventType y Place.

Existe un ArrayCollection para cada una de estas tablas, donde se van guardando las instancias de las entidades.

Se realiza un bucle for que va pasando de evento a evento.  

Por cada evento el sistema comprueba el idioma de éste.  Primero comprueba si el Language del evento ya está dentro del ArrayCollection de Languages.  En caso de estarlo coge esa objeto y se lo aplica al evento.  Si no lo está crea un nuevo objeto Languages, la añade al ArrayCollection y después se lo aplica al evento.  Lo mismo ocurre con los EventTypes.

Con los Territory y Countries funciona un poco distinto.  Primero comprueba si el Town del evento se corresponde con un Town dentro del ArrayCollection de estos.  Si es así, al estar asociado con un Territory y un Country de antemano, no comprueba que estos no existan, porque ya deberían hacerlo.

Los Places funcionan como los idiomas, pero aparte de comprobar si el Place ya existe en su ArrayCollection correspondiente también comprueba si tiene asociado el mismo pueblo, ya que puede (aunque es difícil) haber dos locales con el mismo nombre en distintos pueblos.

Después iteramos sobre cada ArrayCollection para hacer un persist de los datos y hacemos un flush para guardarlos en la DB.  (El flush está comentado para que no de problemas si no hay DB).